package vezdeborg.truecode.m18_permissions

import android.app.Application
import androidx.room.Room
import vezdeborg.truecode.m18_permissions.data.AppDatabase

class App: Application() {
    lateinit var database: AppDatabase

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "MyDatabase").build()
    }
}