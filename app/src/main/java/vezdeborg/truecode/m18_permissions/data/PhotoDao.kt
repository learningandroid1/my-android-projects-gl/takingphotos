package vezdeborg.truecode.m18_permissions.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(photo: Photo)

    // вариант с Flow
    @Query("SELECT * FROM photos")
    fun getAllPhotos(): Flow<List<Photo>>

    // вариант с просто List
    @Query("SELECT * FROM photos")
    suspend fun getListOfAllPhotos(): List<Photo>
}