package vezdeborg.truecode.m18_permissions.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import vezdeborg.truecode.m18_permissions.data.Photo
import vezdeborg.truecode.m18_permissions.databinding.ItemBinding

class PhotosAdapter(private val photos: List<Photo>): RecyclerView.Adapter<PhotosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        val binding = ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PhotosViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        val item = photos.getOrNull(position)
        val context = holder.binding.root.context
        holder.binding.tvDate.text = item?.date
        Glide
            .with(context)
            .load(item?.uri)
            .into(holder.binding.photoImage)
    }

    override fun getItemCount(): Int = photos.size
}

class PhotosViewHolder(val binding: ItemBinding): RecyclerView.ViewHolder(binding.root)