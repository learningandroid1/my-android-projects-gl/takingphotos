package vezdeborg.truecode.m18_permissions.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import vezdeborg.truecode.m18_permissions.R
import vezdeborg.truecode.m18_permissions.data.Photo
import vezdeborg.truecode.m18_permissions.data.PhotoDao

class SharedViewModel(private val dao: PhotoDao) : ViewModel() {

//    private val _photosList = MutableStateFlow<List<Photo>>(emptyList())
//    val photosList = _photosList.asStateFlow()

    // вариант с Flow
    val allPhotos = this.dao.getAllPhotos().stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = emptyList()
    )

//    init {
//        loadPhotos()
//    }
//
//    fun loadPhotos() {
//        viewModelScope.launch {
//            _photosList.value = dao.getListOfAllPhotos()
//        }
//    }

    suspend fun addToDatabase(photo: Photo) {
        dao.insert(photo)
    }
}